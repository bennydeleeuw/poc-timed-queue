var random = require('random')
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var redis = require("redis");
var client = redis.createClient();

var sockets = [];

io.on('connection', function (socket) {
  // let channel_id = socket.handshake.headers['x-channel-id'];

  // if (!sockets[channel_id])
  //   sockets[channel_id] = [];

  // sockets[channel_id].push(socket);
  sockets.push(socket);

  // console.log(sockets[channel_id]);
  socket.on('disconnect', (x, y) => {
    var idx = sockets.indexOf(socket);
    sockets.splice(idx, 1)
  })
});

http.listen(3000, function () {
  console.log('listening on *:3000');
});


client.on("error", function (err) {
  console.log("Error " + err);
});


function findSocket(channel_id, ) {

  var result = []

  // io.emit("message", json)
  sockets.forEach(socket => {
    let socket_channel_id = socket.handshake.headers['x-channel-id'];

    if (socket_channel_id == channel_id) {
      result.push(socket)
    }

  })
  var x = random.int(min = 0, max = result.length -1 )
  return result[x];
}
setInterval(function () {

  client.rpop("message-ready", (error, json) => {


    if (json !== null) {
      var data = JSON.parse(json);

      var channel_id = data.channel;
      var channel_socket = findSocket(channel_id);

      // var channel_sockets = sockets[channel_id];
      // var channel_socket = channel_sockets[getRandomInt(sockets.length)];
      // console.log(channel_socket)
      if (channel_socket && channel_socket.connected) {
        console.log("Emitting")
        channel_socket.emit("message", json);
      }

      //Check channel
      //Check for sockets
      //Send to socket
      //Add to wait for ACK?
      //If no sockets, add back to queue? Add timeout?

    }

  })

}, 10);


// var minutes = 5, the_interval = minutes * 60 * 1000;
setInterval(function () {

  client.rpop("message-queue", (error, json) => {


    if (json !== null) {
      var data = JSON.parse(json);

      if (data && data.timestamp && data.timestamp < new Date().getTime()) {
        // console.log(data)

        // client.publish(data.channel, data)
        client.rpush("message-ready", json);

      } else {
        client.rpush("message-queue", json)
      }

    }

  })

}, 10);

