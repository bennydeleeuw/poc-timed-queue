var redis = require("redis"),
  client = redis.createClient();

client.on("error", function (err) {
  console.log("Error " + err);
});

client.flushdb()

var date = new Date();

var timestamp = date.getTime() + 2000;
// var items = [];
for (var x = 0; x < 100; x++) {
  client.lpush("message-queue", JSON.stringify({
    "channel": "Channel 0",// + x % 2,
    "key": x,
    "data": `item ${x}`,
    "timestamp": timestamp
  }), redis.print);
}